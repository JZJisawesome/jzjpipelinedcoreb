/* Fetch stage
 * Provides instructions from memory as well as the pc of those instructions
*/
module Fetch
#(
	parameter logic [31:0] RESET_VECTOR = 32'h00000000//Address for execution to begin at (must be within RAM)
)
(
	input logic clock,
	input logic reset,
	
	//Stage Control
	input logic stall_fetch,
	input logic stall_decode,
	
	//Stage Inputs
	input logic isControlTransferInstruction,//1 if unconditional jump or sucessfull branch instruction, 0 otherwise
	input logic [31:0] controlTransferPC,//new pc from decode stage for if controlTransferInstruction is 1
	
	//Stage Outputs
	output logic [31:0] pc_decode,//for AUIPC and JAL (though for JAL, the Execute stage will need to add 4)
	output reg [31:0] instruction_decode = 32'h00000013,//instruction at address pc_fetch
	
	//Memory Control Outputs
	output logic en_instructionAddress,//Whether to latch instructionAddress into instruction address
	
	//Memory Input
	input logic [31:0] instructionIn,//Instruction in from memory combinationally
	
	//Memory Output
	output logic [31:0] instructionAddress//Instruction address to latch when en_address is asserted
);

//Clear decode stage on branch mispredict (aka any control transfer instruction because newPC was NOT next seq pc)
logic clear_decode;
assign clear_decode = isControlTransferInstruction;

//From PCController to IMemoryInterface
logic [31:0] newPC;//Output to allow address register inside of memory to be the same as the program counter

//Internal Modules
PCController #(.RESET_VECTOR(RESET_VECTOR)) pcController (.*);
IMemoryInterface iMemoryInterface (.*);

endmodule 
