/* PCController
 * Stores the current PC, calculates the next sequential pc, and chooses between
 * Also provides the PC combinationally to the instruction memory, in order to overcome FPGA
 * limitation that address inputs are synchronous
*/
module PCController
#(
	parameter logic [31:0] RESET_VECTOR = 32'h00000000//Address for execution to begin at (must be within RAM)
)
(
	input logic clock,
	input logic reset,
	
	//Stage Control
	input logic stall_fetch,//hold programCounter constant (don't latch on positive edge)
	input logic stall_decode,//hold pc_fetch constant (don't latch on positive edge)
	//input logic clear_decode,//We don't actually need to clear pc_fetch when this is asserted because it will be ignored anywways (instruction will be nop)
	
	//Stage Inputs
	input logic isControlTransferInstruction,//1 if unconditional jump or sucessfull branch instruction
	input logic [31:0] controlTransferPC,//New pc from decode stage
	
	//Stage Outputs
	output reg [31:0] pc_decode = RESET_VECTOR,//for AUIPC and JAL (though for JAL, the Execute stage will need to add 4)
	
	//Intra-Stage Outputs
	output logic [31:0] newPC//combinational output, latched by address register inside instruction memory
);

//The program counter (register feeding the Fetch stage)
reg [31:0] programCounter = RESET_VECTOR;

//Calculate next sequential pc
logic [31:0] nextSeqPC;
assign nextSeqPC = programCounter + 4;

//newPC multiplexer
assign newPC = isControlTransferInstruction ? controlTransferPC : nextSeqPC;

//Stage output logic
always_ff @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		programCounter <= RESET_VECTOR;
		pc_decode <= RESET_VECTOR;
	end
	else if (clock)
	begin
		if (~stall_fetch)//Only update programCounter if not fetch stalled
			programCounter <= newPC;
			
		if (~stall_decode)//Only update pc_fetch if not fetch stalled
			pc_decode <= programCounter;
	end
end

endmodule