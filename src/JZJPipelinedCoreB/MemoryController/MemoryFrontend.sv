module MemoryFrontend
#(
	parameter INITIAL_MEM_CONTENTS = "initialRam.hex",//File containing initial ram contents (32 bit words)
	parameter int RAM_A_WIDTH = 12//Number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes; maximum of 29
)
(
	input logic clock,
	input logic reset,
	
	//Fetch Stage
	input logic en_instructionAddress,//Whether to latch instructionAddress into instructionAddressRegister
	input logic [31:0] instructionAddress,//Instruction address to latch when en_address is asserted
	
	//Memory Stage (Note, sb and sh not supported)
	input logic [31:0] dataAddress,
	input logic en_memoryWrite,
	
);

endmodule